#!/usr/bin/perl

use strict;
use warnings "all";
use locale;

use FindBin;
BEGIN {
	require "$FindBin::RealBin/../lib/Modules.pm";
}

use Contenido::Globals;
use Contenido::Init;
use ErrorTee;
use PidFile;


# begin
Contenido::Init->init();

my $keeper_module = $state->project.'::Keeper';
$keeper = $keeper_module->new($state);

#PidFile->new($keeper, compat=>1);                # db-based locking (run only on one host)
#PidFile->new($keeper, compat=>1, per_host=>1);   # db-based locking (run on whole cluster)

############################################
# please use:
#     $state->{log_dir} for logging
#     $state->{tmp_dir} for temporary files
###########################################
my $SALES_TIMEOUT = 3600;
my $VAULT_TIMEOUT = 10800;

my $now = Contenido::DateTime->new;
my $request = "UPDATE orders SET manager_id = 0 WHERE status = 1 AND mtime < (CURRENT_DATE - '$SALES_TIMEOUT seconds'::INTERVAL)";
my $sql = $keeper->SQL->prepare( $request );
$sql->execute();
$sql->finish();

$request = "UPDATE orders SET vault_id = 0 WHERE status = 2 AND mtime < (CURRENT_DATE - '$VAULT_TIMEOUT seconds'::INTERVAL)";
$sql = $keeper->SQL->prepare( $request );
$sql->execute();
$sql->finish();
