package webshop::OrderCouponLink;

use base 'Contenido::Link';
use Contenido::Globals;

sub class_name
{
	return 'Купон в заказе';
}

sub class_description
{
	return 'Купон в заказе';
}

sub extra_properties
{
	return (
		{ 'attr' => 'status',	'type' => 'status',	'rusname' => 'Статус купона в заказе',
			'cases' => [
					[0, 'зарегистрирован'],
					[1, 'задействован'],
				],
		},
	);
}

sub class_table
{
	return 'webshop::SQL::OrderCouponLinkTable';
}

sub available_sources
{
	return [ qw(webshop::Order) ];
}

sub available_destinations
{
	return [ qw(webshop::Coupon) ];
}

1;
