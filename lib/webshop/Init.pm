package webshop::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use webshop::Apache;
use webshop::Keeper;


# загрузка всех необходимых плагину классов
# webshop::SQL::SomeTable
# webshop::SomeClass
Contenido::Init::load_classes(qw(
		webshop::SQL::AddressTable
		webshop::SQL::Basket
		webshop::SQL::DeliveryTable
		webshop::SQL::Order
		webshop::SQL::CouponsTable
		webshop::SQL::CouponLinkTable

		webshop::Profile

		webshop::Address
		webshop::Basket
		webshop::Order
		webshop::Delivery
		webshop::Payment
		webshop::Coupon
		webshop::Discount

		webshop::Country
		webshop::Area
		webshop::Town

		webshop::DeliverySection
		webshop::PaymentSection
		webshop::RegionSection

		webshop::CouponItemLink
		webshop::DiscountItemLink
		webshop::OrderCouponLink
	));

sub init {
	push @{ $state->{'available_documents'} }, qw(webshop::Basket webshop::Order webshop::Delivery webshop::Payment webshop::Coupon webshop::Discount webshop::Country webshop::Area webshop::Town webshop::Profile);
	push @{ $state->{'available_sections'} }, qw(webshop::DeliverySection webshop::PaymentSection webshop::RegionSection);
	push @{ $state->{'available_links'} }, qw(webshop::CouponItemLink webshop::OrderCouponLink);
	0;
}

1;
