package webshop::Address;

use Contenido::Globals;
use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус заказа',
			'cases' => [
					[0, 'Дополнительный адрес'],
					[1, 'Основной адрес'],
				],
		},
		{ 'attr' => 'name',		'type' => 'string',	'rusname' => 'Контактное лицо' },
		{ 'attr' => 'phone',		'type' => 'string',	'rusname' => 'Телефон для связи',
				mandatory => 1, rel => 'Не указан телефон' },
		{ 'attr' => 'zipcode',		'type' => 'string',	'rusname' => 'Почтовый индекс',
				mandatory => 1, rel => 'Не указан почтовый индекс' },
		{ 'attr' => 'area',		'type' => 'lookup',	'rusname' => 'Регион',
				lookup_opts => { class => 'webshop::Area', order_by => 'name' },
				allow_null => 1, mandatory => 1, rel => 'Не выбран регион'
		},
		{ 'attr' => 'town_id',		'type' => 'lookup',	'rusname' => 'Город',
				lookup_opts => { class => 'webshop::Town', order_by => 'name' },
				allow_null => 1, mandatory => 1, rel => 'Не выбран город'
		},
		{ 'attr' => 'town',		'type' => 'string',	'rusname' => 'Город',
				mandatory => 1, rel => 'Не указан город' },
		{ 'attr' => 'carrier',		'type' => 'string',	'rusname' => 'Транспортная компания' },
		{ 'attr' => 'metro',		'type' => 'string',	'rusname' => 'Ближайшее метро' },
		{ 'attr' => 'address',		'type' => 'text',       'rusname' => 'Адрес доставки', rows => 5,
				mandatory => 1, rel => 'Не заполнен адрес доставки' },
		{ 'attr' => 'timeline',		'type' => 'string',	'rusname' => 'Предпочтительная дата и время',
				mandatory => 1, rel => 'Вы не указали предпочтительное время получения заказа' },
		{ 'attr' => 'description',	'type' => 'text',       'rusname' => 'Описание для курьера', rows => 5 },
	)
}

sub class_name
{
	return 'Webshop: адрес доставки';
}

sub class_description
{
	return 'Webshop: адрес доставки';
}

sub class_table
{
	return 'webshop::SQL::AddressTable';
}

sub as_string
{
    my ($self, %opts) = @_;
    my $areas = delete $opts{areas};

    my @string;
    push @string, $self->name		if $self->name;
    push @string, $self->zipcode	if $self->zipcode;
    if ( $self->area ) {
	if ( ref $areas ) {
		push @string, $areas->{$self->area}->name	if exists $areas->{$self->area};
	} else {
		my $area = $self->keeper->get_document_by_id( $self->area, class => 'webshop::Area' );
		push @string, $area->name	if ref $area;
	}
    }
    push @string, $self->town		if $self->town;
    push @string, $self->address	if $self->address;
    return join(', ', @string);
}

sub pre_store
{
	my $self = shift;

	if ( $self->town_id && !$self->town ) {
		my $town = webshop::Town->new( $keeper, $self->town_id );
		if ( ref $town ) {
			$self->town( $town->name );
		}
	}

	return 1;
}

1;
