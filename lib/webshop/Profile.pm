package webshop::Profile;

use strict;
use Contenido::Globals;
use base 'Contenido::Document';

sub extra_properties
{
	return (
		{ 'attr' => 'dtime',					'hidden' => 1 },
		{ 'attr' => 'status',		'type' => 'status',
			cases => [
				[0, 'Профиль не активен'],
				[1, 'Профиль активен'],
			],
		},
	)
}

sub class_name
{
	return 'Настройки веб-магазина';
}

sub class_description
{
	return 'Профиль настроек веб-магазина';
}

1;
