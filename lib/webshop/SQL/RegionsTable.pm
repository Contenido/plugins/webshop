package webshop::SQL::RegionsTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'webshop_regions';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter

					_pid_filter
					_alias_filter
			)];

sub available_filters {
	return $available_filters;
}


# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = grep { $_->{attr} ne 'dtime' } $self->SUPER::required_properties;
	return (
		@parent_properties,
		{							# Parent ID
			'attr'		=> 'pid',
			'type'		=> 'integer',
			'rusname'	=> 'ID родительского региона',
			'db_field'	=> 'pid',
			'db_type'	=> 'integer',
			'db_opts'	=> "default 0",
			'default'       => 0,
		},
		{
			'attr'		=> 'alias',
			'type'		=> 'string',
			'rusname'	=> 'Web-alias региона',
			'db_field'	=> 'alias',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'price',
			'type'		=> 'string',
			'rusname'	=> 'Стоимость доставки',
			'db_field'	=> 'price',
			'db_type'	=> 'text',
		},
	);
}

########### FILTERS DESCRIPTION ###############################################################################
sub _pid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{pid} );
	return &SQL::Common::_generic_int_filter('d.pid', $opts{pid});
}

sub _alias_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{alias} );
	return &SQL::Common::_generic_text_filter('d.alias', $opts{alias});
}

1;