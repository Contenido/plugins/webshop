package webshop::SQL::AddressTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'adresses';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter

					_uid_filter
					_delivery_id_filter
					_active_delivery_filter
			)];

sub available_filters {
	return $available_filters;
}


# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        my $self = shift;

        my @parent_properties = grep { $_->{attr} ne 'sections' } $self->SUPER::required_properties;
        return (
                @parent_properties,
		{
			'attr'		=> 'uid',
			'type'		=> 'string',
			'rusname'	=> 'ID пользователя',
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{
			'attr'		=> 'delivery',
			'type'		=> 'lookup',
			'rusname'	=> 'Доставка',
			'lookup_opts'   => {
				'class'		=> 'webshop::Delivery',
			},
			'db_field'	=> 'delivery_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
		},
		{
			'attr'		=> 'sections',
			'type'		=> 'sections_list',
			'rusname'	=> 'Секции',
			'hidden'	=> 1,
			'db_field'	=> 'sections',
			'db_type'	=> 'integer',
                },
	);
}

########### FILTERS DESCRIPTION ###############################################################################
sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _delivery_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{delivery_id} );
	return &SQL::Common::_generic_int_filter('d.delivery_id', $opts{delivery_id});
}

sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}

sub _active_delivery_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{active} && $opts{active} );
	return ([], [], " join webshop_delivery as l on l.id=d.delivery_id and l.status > 0 ");
}

1;