package webshop::SQL::CouponsTable;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'webshop_coupons';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter
					_link_filter

					_uid_filter
					_pid_filter
					_code_filter
					_interval_filter
			)];

sub available_filters {
	return $available_filters;
}


# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        my $self = shift;

        my @parent_properties = grep { $_->{attr} ne 'sections' && $_->{attr} ne 'dtime' } $self->SUPER::required_properties;
        return (
                @parent_properties,
		{
			'attr'		=> 'code',
			'type'		=> 'string',
			'rusname'	=> 'Код купона',
			'shortname'	=> 'Код',
			'column'	=> 1,
			'rem'		=> 'Оставьте незаполненным для автоматической генерации',
			'db_field'	=> 'code',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'dtime',
			'type'		=> 'datetime',
			'rusname'	=> 'Начало действия купона',
			'shortname'	=> 'Начало',
			'column'	=> 5,
			'db_field'	=> 'dtime',
			'db_type'	=> 'timestamp',
			'db_opts'	=> 'not null default now()',
			'default'	=> 'CURRENT_TIMESTAMP',
		},
		{
			'attr'		=> 'etime',
			'type'		=> 'datetime',
			'rusname'	=> 'Окончание действия купона',
			'shortname'	=> 'Конец',
			'column'	=> 6,
			'db_field'	=> 'etime',
			'db_type'	=> 'timestamp',
			'db_opts'	=> 'not null default now()',
			'default'	=> 'CURRENT_TIMESTAMP',
		},
		{
			'attr'		=> 'pid',
			'type'		=> 'integer',
			'rusname'	=> 'ID прототипа',
			'hidden'	=> 1,
			'db_field'	=> 'pid',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{						       # User ID
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'ID пользователя',
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{
			'attr'		=> 'groups',
			'type'		=> 'lookup_multi',
			'rusname'	=> 'Группы товаров',
			'db_field'	=> 'groups',
			'db_type'	=> 'integer[]',
		},
		{
			'attr'          => 'sections',
			'type'          => 'sections_list',
			'rusname'       => 'Секции',
			'hidden'        => 1,
			'db_field'      => 'sections',
			'db_type'       => 'integer',
		},
	);
}

########### FILTERS DESCRIPTION ###############################################################################
sub _s_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{s} );
	return &SQL::Common::_generic_int_filter('d.sections', $opts{s});
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _pid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{pid} );
	return &SQL::Common::_generic_int_filter('d.pid', $opts{pid});
}

sub _code_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{code} );
	return &SQL::Common::_generic_name_filter('d.code', $opts{code}, undef, \%opts);
}

sub _interval_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{interval} && ref $opts{interval} eq 'ARRAY' && scalar @{$opts{interval}} == 2 );
	return undef unless $opts{interval}->[0] && $opts{interval}->[1];
	my $date1 = ref $opts{interval}->[0] ? $opts{interval}->[0] : Contenido::DateTime->new( postgres => $opts{interval}->[0].( $opts{interval}->[0] =~ /\d{2}\:\d{2}\:\d{2}/ ? '' : ' 00:00:00' ) );
	my $date2 = ref $opts{interval}->[1] ? $opts{interval}->[1] : Contenido::DateTime->new( postgres => $opts{interval}->[1].( $opts{interval}->[1] =~ /\d{2}\:\d{2}\:\d{2}/ ? '' : ' 00:00:00' ) );
	return undef if DateTime->compare($date1, $date2) == 1;

	my $wheres = "date_trunc('day', d.dtime) <= ? and date_trunc('day', d.etime) >= ?";
	my @values = ($date2->ymd('-'), $date1->ymd('-'));

	return ($wheres, \@values);
}

sub _link_filter {
	my ($self,%opts)=@_;

	my @wheres=();
	my @binds=();

	# Связь определенного класса
	if (exists($opts{lclass})) {
		my ($where, $values) = SQL::Common::_generic_text_filter('l.class', $opts{lclass});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	my $lclass = $opts{lclass} || 'Contenido::Link';
	my $link_table = $lclass->_get_table->db_table();

	# Ограничение по статусу связи
	if ( exists $opts{lstatus} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.status', $opts{lstatus});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	# Ограничение по uid
	if ( exists $opts{luid} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.uid', $opts{luid});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	# Ограничение по сессии
	if ( exists $opts{lsession} ) {
		my ($where, $values) = SQL::Common::_generic_text_filter('l.session', $opts{lsession});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
	}

	# Связь с определенным документ(ом/тами) по цели линка
	if ( exists $opts{ldest} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.dest_id', $opts{ldest});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ($self->_single_class) {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.source_id=d.id and l.source_class=d.class");
		}
	}
	# Связь с определенным документ(ом/тами) по источнику линка
	if ( exists $opts{lsource} ) {
		my ($where, $values) = SQL::Common::_generic_int_filter('l.source_id', $opts{lsource});
		push (@wheres, $where);
		push (@binds,   ref($values) ? @$values:$values) if (defined $values);
		if ($self->_single_class) {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id");
		} else {
			return (\@wheres, \@binds, " join $link_table as l on l.dest_id=d.id and l.dest_class=d.class");
		}
	}

        return (undef);
}


1;