package webshop::SQL::Order;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'orders';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter
					_link_filter

					_payment_filter
					_uid_filter
					_company_filter
					_manager_filter
					_vault_filter
					_postman_filter
					_sync_filter
			)];

sub available_filters {
	return $available_filters;
}

my @required_properties = (
		{						       # Идентификатор документа, сквозной по всем типам...
			'attr'		=> 'id',
			'type'		=> 'integer',
			'rusname'	=> 'Идентификатор документа',
			'hidden'	=> 1,
			'readonly'	=> 1,
			'auto'		=> 1,
			'db_field'	=> 'id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default nextval('public.documents_id_seq'::text)",
		},
		{						       # Класс документа...
			'attr'		=> 'class',
			'type'		=> 'string',
			'rusname'	=> 'Класс документа',
			'hidden'	=> 1,
			'readonly'	=> 1,
			'db_field'	=> 'class',
			'db_type'	=> 'varchar(48)',
			'db_opts'	=> 'not null',
		},
		{						       # Название (не используется)
			'attr'		=> 'name',
			'type'		=> 'string',
			'rusname'	=> 'Имя заказчика',
			'column'	=> 2,
			'db_field'	=> 'name',
			'db_type'	=> 'varchar(255)',
		},
		{						       # User ID
			'attr'		=> 'uid',
			'type'		=> 'string',
			'rusname'	=> 'ID пользователя',
#			'hidden'        => 1,
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{
			'attr'		=> 'company_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID компании',
			'hidden'        => 1,
			'db_field'	=> 'company_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{						       # User ID
			'attr'		=> 'manager_id',
			'type'		=> 'string',
			'rusname'	=> 'ID менеджера',
			'hidden'        => 1,
			'db_field'	=> 'manager_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{						       # User ID
			'attr'		=> 'vault_id',
			'type'		=> 'string',
			'rusname'	=> 'ID специалиста комплектации',
			'hidden'        => 1,
			'db_field'	=> 'vault_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{						       # User ID
			'attr'		=> 'postman_id',
			'type'		=> 'string',
			'rusname'	=> 'ID курьера',
			'hidden'        => 1,
			'db_field'	=> 'postman_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null default 0",
			'default'       => 0,
		},
		{						       # Время создания документа, служебное поле...
			'attr'          => 'ctime',
			'type'          => 'datetime',
			'rusname'       => 'Время создания',
			'readonly'      => 1,
			'auto'          => 1,
			'hidden'        => 1,
			'db_field'      => 'ctime',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{						       # Время модификации документа, служебное поле...
			'attr'          => 'mtime',
			'type'          => 'datetime',
			'rusname'       => 'Время модификации',
			'hidden'        => 1,
			'auto'          => 1,
			'db_field'      => 'mtime',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
		},
		{							# Дата и время документа...
			'attr'          => 'dtime',
			'type'          => 'datetime',
			'rusname'       => 'Заказ подтвержден',
			'shortname'	=> 'Дата',
			'column'        => 1,
			'postshow'	=> 1,
			'facilshow'	=> 1,
			'db_field'      => 'dtime',
			'db_type'       => 'timestamp',
			'db_opts'       => 'not null default now()',
			'default'       => 'CURRENT_TIMESTAMP',
                },
		{						       # Массив секций, обрабатывается специальным образом...
			'attr'          => 'sections',
			'type'          => 'sections_list',
			'rusname'       => 'Секции',
			'hidden'        => 1,
			'db_field'      => 'sections',
			'db_type'       => 'integer[]',
		},
		{						       # Одно поле статуса является встроенным...
			'attr'          => 'status',
			'type'          => 'status',
			'postshow'	=> 1,
			'facilshow'	=> 1,
			'column'	=> 7,
			'rusname'       => 'Статус',
			'db_field'      => 'status',
			'db_type'       => 'integer',
		},
		{
			'attr'          => 'payment',
			'type'          => 'status',
			'postshow'	=> 1,
			'facilshow'	=> 1,
			'column'	=> 8,
			'cases'	=> [
				[0, 'Не оплачен'],
				[1, 'Оплачен'],
				[2, 'Частично'],
				[3, 'Средства заблокированы'],
				[4, 'Возврат'],
			],
			'rusname'       => 'Оплачен',
			'db_field'      => 'payment',
			'db_type'       => 'integer',
		},
		{
			'attr'          => 'syncro',
			'type'          => 'checkbox',
			'postshow'	=> 1,
			'facilshow'	=> 1,
			'rusname'       => 'Синхронизован',
			'db_field'      => 'syncro',
			'db_type'       => 'integer',
			'default'	=> 0,
		},
		{
			'attr'          => 'synctime',
			'type'          => 'datetime',
			'rusname'       => 'Время последней синхронизации',
			'postshow'	=> 1,
			'facilshow'	=> 1,
			'db_field'      => 'synctime',
			'db_type'       => 'timestamp',
                },
);

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
        return @required_properties;
}

########### FILTERS DESCRIPTION ###############################################################################
sub _get_orders {
	my ($self, %opts) = @_;

	if ($opts{order_by}) {
		return ' order by '.$opts{order_by};
	} else {
		return ' order by id desc';
	}
	return undef;
}

sub _payment_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{payment} );
	return &SQL::Common::_generic_int_filter('d.payment', $opts{payment});
}

sub _sync_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{sync} );
	return &SQL::Common::_generic_int_filter('d.syncro', $opts{sync});
}

sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _company_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{company_id} );
	return &SQL::Common::_generic_int_filter('d.company_id', $opts{company_id});
}

sub _manager_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{manager} );
	return &SQL::Common::_generic_int_filter('d.manager_id', $opts{manager});
}

sub _vault_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{vault} );
	return &SQL::Common::_generic_int_filter('d.vault_id', $opts{vault});
}

sub _postman_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{postman} );
	return &SQL::Common::_generic_int_filter('d.postman_id', $opts{postman});
}

1;