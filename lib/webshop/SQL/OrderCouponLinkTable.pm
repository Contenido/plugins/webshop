package webshop::SQL::OrderCouponLinkTable;

use base 'SQL::LinkTable';
use Contenido::Globals;

sub db_table
{
	return 'webshop_order_coupons';
}

my $available_filters = [qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
					_class_excludes_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter

					_dest_id_filter
					_source_id_filter
					_source_class_filter
					_dest_class_filter

					_uid_filter
					_session_filter
			)];

sub available_filters {
	return $available_filters;
}


# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = $self->SUPER::required_properties;
	return (
		@parent_properties,
		{							# User ID
			'attr'		=> 'uid',
			'type'		=> 'pickup',
			'rusname'	=> 'ID пользователя',
			'lookup_opts'	=> {
				class		=> $state->{users}->profile_document_class,
				order_by	=> 'email',
				search_by	=> 'email'
			},
			'db_field'	=> 'uid',
			'db_type'	=> 'integer',
			'db_opts'	=> "default 0",
			'default'	=> 0,
		},
		{							# ID Сессии
			'attr'          => 'session',
			'type'          => 'string',
			'rusname'       => 'ID Сессии пользователя',
			'db_field'      => 'session',
			'db_type'       => 'text',
		},
	);
}

########### FILTERS DESCRIPTION ###############################################################################
sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _session_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{session} );
	return &SQL::Common::_generic_text_filter('d.session', $opts{session});
}

1;