package webshop::SQL::Basket;

use base 'SQL::DocumentTable';

sub db_table
{
	return 'basket';
}


sub available_filters {
	my @available_filters = qw(

					_class_filter
					_status_filter
					_in_id_filter
					_id_filter
					_name_filter
				        _class_excludes_filter
					_sfilter_filter
					_excludes_filter
					_datetime_filter
					_date_equal_filter
					_date_filter
					_previous_days_filter
					_s_filter

					_color_filter
					_size_filter
					_session_filter
					_uid_filter
					_order_id_filter
					_item_filter
			);

	return \@available_filters;
}

# ----------------------------------------------------------------------------
# Свойства храним в массивах, потому что порядок важен!
# Это общие свойства - одинаковые для всех документов.
#
#   attr - обязательный параметр, название атрибута;
#   type - тип аттрибута, требуется для отображдения;
#   rusname - русское название, опять же требуется для отображения;
#   hidden - равен 1, когда
#   readonly - инициализации при записи только без изменения в дальнейшем
#   db_field - поле в таблице
#   default  - значение по умолчанию (поле всегда имеет это значение)
# ----------------------------------------------------------------------------
sub required_properties
{
	my $self = shift;

	my @parent_properties = $self->SUPER::required_properties;
	return (
		@parent_properties,
		{
			'attr'		=> 'uid',
			'type'		=> 'integer',
			'rusname'	=> 'Идентификатор пользователя',
			'lookup_opts'	=> {
				'class'		=> $self->profile_class(),
				'search_by'	=> 'login',
			},
			'db_field'      => 'uid',
			'db_type'       => 'integer',
			'db_opts'       => "default 0",
		},
		{						       # ID товара
			'attr'		=> 'item_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID товара',
			'hidden'	=> 1,
			'db_field'	=> 'item_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "not null",
		},
		{
			'attr'		=> 'item_class',
			'type'		=> 'string',
			'rusname'	=> 'Класс товарной позиции',
			'hidden'	=> 1,
			'db_field'	=> 'item_class',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'item_table',
			'type'		=> 'string',
			'rusname'	=> 'Класс таблицы товарной позиции',
			'hidden'	=> 1,
			'db_field'	=> 'item_table',
			'db_type'	=> 'text',
		},
		{
			'attr'		=> 'number',
			'type'		=> 'integer',
			'rusname'	=> 'Количество позиций',
			'db_field'	=> 'number',
			'db_type'	=> 'integer',
			'db_opts'	=> "default 0",
		},
		{
			'attr'		=> 'price',
			'type'		=> 'string',
			'rusname'	=> 'Цена позиции',
			'db_field'	=> 'price',
			'db_type'	=> 'float',
			'db_opts'	=> "default 0",
		},
		{
			'attr'		=> 'special_price',
			'type'		=> 'checkbox',
			'rusname'	=> 'Акция! (скидки не действуют)',
			'db_field'	=> 'special_price',
			'db_type'	=> 'integer',
			'db_opts'	=> "default 0",
		},
		{						       # ID заказа
			'attr'		=> 'order_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID заказа',
			'hidden'	=> 1,
			'db_field'	=> 'order_id',
			'db_type'	=> 'integer',
			'db_opts'	=> "default 0",
		},
		{						       # ID цвета
			'attr'		=> 'color_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID цвета',
			'db_field'	=> 'color_id',
			'db_type'	=> 'integer',
		},
		{						       # ID размера
			'attr'		=> 'size_id',
			'type'		=> 'integer',
			'rusname'	=> 'ID размера',
			'hidden'	=> 1,
			'db_field'	=> 'size_id',
			'db_type'	=> 'integer',
		},
		{						       # ID Сессии
			'attr'		=> 'session',
			'type'		=> 'string',
			'rusname'	=> 'ID Сессии пользователя',
			'db_field'	=> 'session',
			'db_type'	=> 'text',
		},
	);
}


sub profile_class () {
	my $self = shift;
	return ref $self->{keeper} ? $self->{keeper}{webshop}{state}->profile_document_class : undef;
}


########### FILTERS DESCRIPTION ###############################################################################
sub _uid_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{uid} );
	return &SQL::Common::_generic_int_filter('d.uid', $opts{uid});
}

sub _session_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{session} );
	if ( $opts{session} && !ref $opts{session} ) {
		my $que = '(d.session IN (?) AND (d.uid IS NULL OR d.uid = 0))';
		return $que, [$opts{session}];
	} else {
		return &SQL::Common::_generic_text_filter('d.session', $opts{session});
	}
}

sub _item_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{item} );
	return &SQL::Common::_generic_int_filter('d.item_id', $opts{item});
}

sub _color_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{color} );
	return &SQL::Common::_generic_int_filter('d.color_id', $opts{color});
}

sub _size_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{size} );
	return &SQL::Common::_generic_int_filter('d.size_id', $opts{size});
}

sub _order_id_filter {
	my ($self,%opts)=@_;
	return undef unless ( exists $opts{order_id} );
	return &SQL::Common::_generic_int_filter('d.order_id', $opts{order_id});
}


1;