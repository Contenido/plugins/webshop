package webshop::Delivery;

use base "Contenido::Document";
use JSON::XS;
our $json = JSON::XS->new->utf8;

sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',			'rusname' => 'Статус',
			'cases' => [
					[0, 'Не активна'],
					[1, 'Действует'],
				],
		},
		{ 'attr' => 'alias',		'type' => 'string',		'rusname' => 'Код (alias) поставщика' },
		{ 'attr' => 'dtime',						'hidden' => 1, column => undef },
		{ 'attr' => 'price',		'type' => 'string',		'rusname' => 'Стоимость доставки (число)', default => 0, column => 4 },
		{ 'attr' => 'price_modify',	'type' => 'cost_level',		'rusname' => 'Стоимость доставки с учетом стоимости заказа' },
		{ 'attr' => 'price_region',	'type' => 'cost_region',	'rusname' => 'Стоимость доставки с учетом региона' },
		{ 'attr' => 'abstr',		'type' => 'text',		'rusname' => 'Краткое описание' },
		{ 'attr' => 'fields',		'type' => 'status_multi',	'rusname' => 'Поля, необходимые к заполнению',
			'cases'	=> [
				['timeline','Время заказа'],
				['zipcode','Почтовый индекс'],
				['country','Страна'],
				['area','Регион из списка'],
				['town_id','Город из списка'],
				['town','Город'],
				['address','Адрес'],
				['metro','Ближайшее метро'],
				['carrier','Название логистической компании'],
				['axiomus','Аксиомус Виджет'],
			],
		},
	)
}

sub price_actual
{
    my $self = shift;
    my (%opts) = @_;

    my $sum = delete $opts{sum};
    my $region_id = exists $opts{region} && ref $opts{region} ? $opts{region}->id : exists $opts{region_id} ? $opts{region_id} : undef;
    my $price = $self->price;
    $price = defined $price && $price =~ /^\d+$/ ? $price : 'не определена';
   
    if ( $region_id ) {
	if ( $self->price_region ) {
		my $region_price = $json->decode( $self->price_region );
		if ( ref $region_price eq 'HASH' && exists $region_price->{$region_id} ) {
			$price = $region_price->{$region_id};
		}	
	}
    } elsif ( $sum ) {
	if ( $self->price_modify ) {
		my $price_modify = $json->decode( $self->price_modify );
		if ( ref $price_modify eq 'ARRAY' && @$price_modify ) {
			foreach my $mod ( @$price_modify ) {
				if ( $mod->{level} <= $sum ) {
					if ( $mod->{cost} =~ /(\d+)%/ ) {
						my $mod = $1;
						$price = sprintf("%.2f", ($self->price * $mod) / 100);
					} else {
						$price = $mod->{cost};
					}
				}
			}
		}
	}
    }
    return $price;
}

sub get_fields
{
    my $self = shift;
    my $fields = $self->fields;
    my @fields = @$fields	if $fields && ref $fields eq 'ARRAY';

    return @fields;
}

sub check_field
{
    my $self = shift;
    my $field = shift;
    return	unless $field;
    my $fields = $self->fields;
    my %fields = map { $_ => 1 } @$fields	if ref $fields eq 'ARRAY' && @$fields;

    return exists $fields{$field} ? 1 : 0;
}

sub class_name
{
	return 'Способ доставки';
}

sub class_description
{
	return 'Способ доставки';
}

sub class_table
{
	return 'webshop::SQL::DeliveryTable';
}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{delivery}	if ref $project->s_alias eq 'HASH';
	my $sections = $self->{sections};
	if ( $default_section ) {
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($default_section, $sections);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}

1;
