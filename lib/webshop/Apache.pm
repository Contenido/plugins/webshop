package webshop::Apache;

use strict;
use warnings 'all';

use webshop::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{webshop} = webshop::Keeper->new($state->webshop);
}

sub request_init {
}

sub child_exit {
}

1;
