package webshop::Order;

use base "Contenido::Document";
use Contenido::Globals;

sub extra_properties
{
	return (
		{ 'attr' => 'name',	'type' => 'string',	postedit => 1, faciledit => 1 },
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус заказа', facilshow => 1, postshow => 1,
			'cases' => [
					[0, 'открыт'],
					[1, 'обрабатывается'],
					[2, 'комплектуется'],
					[3, 'в доставке'],
					[4, 'доставлен'],
					[5, 'отклонен'],
					[6, 'ожидание оплаты'],
					[7, 'товар в резерве'],
					[8, 'не дозвонились'],
					[-1, 'удален'],
				],
		},
		{ 'attr' => 'delivery',		'type' => 'lookup',	'rusname' => 'Тип доставки', facilshow => 1, postshow => 1,
			'allow_null'	=> 1,
			'lookup_opts'   => {
				'class'         => 'webshop::Delivery',
			},
		},
		{ 'attr' => 'pay_by',		'type' => 'lookup',	'rusname' => 'Тип оплаты', facilshow => 1, postshow => 1,
			'allow_null'	=> 1,
			'lookup_opts'   => {
				'class'         => 'webshop::Payment',
			},
		},
		{ 'attr' => 'pay_alias',	'type' => 'string',	'rusname' => 'Название метода оплаты', shortname => 'Оплата',
				postshow => 1, facilshow => 1 },
		{ 'attr' => 'pay_amount',	'type' => 'string',	'rusname' => 'Оплаченная сумма',
				postshow => 1, facilshow => 1 },
		{ 'attr' => 'btime',		'type' => 'datetime',	'rusname' => 'Заказ отдан на комплектацию',
				postshow => 1, facilshow => 1 },
		{ 'attr' => 'ftime',		'type' => 'datetime',	'rusname' => 'Заказ передан в доставку',
				manager_hidden => 1, postshow => 1, faciledit => 1 },
		{ 'attr' => 'etime',		'type' => 'datetime',	'rusname' => 'Заказ доставлен',
				manager_hidden => 1, postedit => 1, facilshow => 1 },
		{ 'attr' => 'num',		'type' => 'integer',	'rusname' => 'Количество позиций',
				manager_hidden => 1, manshow => 1, postshow => 1, facilshow => 1 },
		{ 'attr' => 'sum',		'type' => 'string',	'rusname' => 'Сумма (gross)', shortname => 'Сумма',
				manager_hidden => 1, column => 5, postshow => 1, facilshow => 1 },
		{ 'attr' => 'sum_discount',		'type' => 'string',	'rusname' => 'Сумма скидки', shortname => 'Скидка',
				manager_hidden => 1, column => 6, postshow => 1, facilshow => 1 },
		{ 'attr' => 'sum_delivery',	'type' => 'string',	'rusname' => 'Стоимость доставки', shortname => 'Доставка',
				manager_hidden => 1, column => 7, postshow => 1, facilshow => 1 },
		{ 'attr' => 'sum_total',	'type' => 'string',	'rusname' => 'Сумма общая', shortname => 'Total',
				manager_hidden => 1, column => 8, virtual => 1, postshow => 1, facilshow => 1 },
		{ 'attr' => 'contact',		'type' => 'string',	'rusname' => 'Контактное лицо', facilshow => 1 },
		{ 'attr' => 'email',		'type' => 'string',	'rusname' => 'E-mail для связи', shortname => 'E-mail',
				column => 3, postshow => 1, facilshow => 1, mandatory => 1, },
		{ 'attr' => 'phone',		'type' => 'string',	'rusname' => 'Телефон для связи', shortname => 'Тел.',
				column => 4, postshow => 1, facilshow => 1,
				mandatory => 1, rel => 'Не указан телефон' },
		{ 'attr' => 'address',		'type' => 'text',       'rusname' => 'Адрес доставки', rows => 5, postedit => 1, facilshow => 1,
				mandatory => 1, rel => 'Не заполнен адрес доставки' },
		{ 'attr' => 'zipcode',		'type' => 'string',	'rusname' => 'Почтовый индекс', postshow => 1,
				mandatory => 1, rel => 'Не указан почтовый индекс' },
		{ 'attr' => 'area',		'type' => 'lookup',	'rusname' => 'Регион',
				lookup_opts => { class => 'webshop::Area', order_by => 'name' },
				allow_null => 1, mandatory => 1, rel => 'Не выбран регион'
		},
		{ 'attr' => 'town_id',		'type' => 'lookup',	'rusname' => 'Город',
				lookup_opts => { class => 'webshop::Town', order_by => 'name' },
				allow_null => 1, mandatory => 1, rel => 'Не выбран город'
		},
		{ 'attr' => 'town',		'type' => 'string',	'rusname' => 'Город', postshow => 1, facilshow => 1,
				mandatory => 1, rel => 'Не указан город' },
		{ 'attr' => 'carrier',		'type' => 'string',	'rusname' => 'Транспортная компания', postshow => 1 },
		{ 'attr' => 'metro',		'type' => 'string',	'rusname' => 'Ближайшее метро', manager_hidden => 1, postshow => 1 },
		{ 'attr' => 'timeline',		'type' => 'string',	'rusname' => 'Предпочтительная дата и время', postshow => 1, },
		{ 'attr' => 'description',	'type' => 'text',       'rusname' => 'Описание для курьера', rows => 5, postshow => 1, faciledit => 1 },
		{ 'attr' => 'manager_comment',	'type' => 'text',       'rusname' => 'Комментарий менеджера', rows => 5, postedit => 1, faciledit => 1 },
		{ 'attr' => 'facility_comment',	'type' => 'text',       'rusname' => 'Описание проблем с комплектацией', rows => 5, manager_hidden => 1, faciledit => 1 },
		{ 'attr' => 'delivery_comment',	'type' => 'text',       'rusname' => 'Описание проблем с доставкой', rows => 5, manager_hidden => 1, postedit => 1 },
	)
}

sub recount {
	my $self = shift;
	my $basket = $self->keeper->{webshop}->get_basket( order_id => $self->id );
	my $store = 0;
	if ( ref $basket && @$basket ) {
		my $sum = 0;
		foreach my $bi ( @$basket ) {
			$sum += $bi->price * $bi->number;
		}
		if ( $sum != $self->sum ) {
			$self->sum( $sum );
			$store = 1;
		}
		my $coupons = $self->keeper->get_documents(
				class	=> 'webshop::Coupon',
				lclass	=> 'webshop::OrderCouponLink',
			);
	}
	if ( $store ) {
		$self->store;
	}
}

sub sum_total {
	my $self = shift;

	return ($self->sum || 0) - ($self->sum_discount || 0) + ($self->sum_delivery || 0);
}


sub sum_formatted {
	my $self = shift;

	my $price = $self->sum;
	$price = reverse $price;
	$price =~ s/(\d{3})/$1\ /g;
	$price = reverse $price;

	return $price;
}

sub total_formatted {
	my $self = shift;

	my $price = $self->sum_total;
	return $keeper->{webshop}->price_format( $price );
}


sub reject {
	my $self = shift;

#	my $query = $self->{keeper}->SQL->prepare("delete from basket where order_id = ? and uid = ?");
#	$query->execute( $self->id, $self->uid );

	$self->status(5);
	$self->store;
}


sub class_name
{
	return 'Заказ';
}

sub class_description
{
	return 'Заказ';
}

sub class_table
{
	return 'webshop::SQL::Order';
}

sub table_links
{
	return [
		{ name => 'Корзина', class => 'webshop::Basket', filter => 'order_id', field => 'order_id' },
	];
}

sub post_store
{
    my $self = shift;

    if ( grep { $_ eq 'payments' } split /\s+/, $state->plugins ) {
	my $action;
	my ($lastop) = $keeper->get_documents( class => 'payments::Operation', order_id => $self->id, order_by => 'id desc', limit => 1 );
	if ( ref $lastop ) {
		if ( $self->status == 1 || $self->status == 2 || $self->status == 6 ) {
			if ( $lastop->name eq 'suspend' || $lastop->name eq 'cancel' || $lastop->name eq 'close' ) {
				$action = 'resume';
			}
		} elsif ( $self->status == 7 ) {
			if ( $lastop->name ne 'suspend' ) {
				$action = 'suspend';
			}
		} elsif ( $self->status == 4 ) {
			if ( $lastop->name ne 'close' ) {
				$action = 'close';
			}
		} elsif ( $self->status == 5 ) {
			if ( $lastop->name ne 'cancel' ) {
				$action = 'cancel';
			}
		}
	} else {
		if ( $self->status == 1 || $self->status == 2 || $self->status == 6 ) {
			$action = 'create';
		} elsif ( $self->status == 7 ) {
			$action = 'suspend';
		} elsif ( $self->status == 4 ) {
			$action = 'close';
		} elsif ( $self->status == 5 ) {
			$action = 'cancel';
		}
	}
	if ( $action && exists $keeper->{payments} ) {
		my $op = $keeper->{payments}->add( order => $self, type => $action, ref $user ? (uuid => $user->id) : () );
	}
    }
    return 1;
}


sub post_delete
{
    my $self = shift;
    my @items = $self->keeper->get_documents(
			class	=> 'webshop::Basket',
			order_id	=> $self->id,
		);
    foreach my $item ( @items ) {
	$item->delete( attachments => 1 );
    }
    my $sql = $self->keeper->SQL->prepare('DELETE FROM webshop_order_coupons where source_id = ?');
    $sql->execute( $self->id );

    1;
}

1;
