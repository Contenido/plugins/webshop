package webshop::Payment;

use base "Contenido::Document";

sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',			'rusname' => 'Статус',
			'cases' => [
					[0, 'Не активен'],
					[1, 'Действует'],
					[2, 'Отладка'],
				],
		},
		{ 'attr' => 'dtime',						'hidden' => 1, column => undef },
		{ 'attr' => 'class',						'column' => undef },
		{ 'attr' => 'alias',		'type' => 'string',		'column' => undef, 'rusname' => 'Алиас метода оплаты' },
		{ 'attr' => 'abstr',		'type' => 'text',		'rusname' => 'Краткое описание' },
		{ 'attr' => 'online',		'type' => 'checkbox',		'rusname' => 'Онлайн-оплата', column => 4 },
		{ 'attr' => 'url',		'type' => 'string',		'rusname' => 'URL перехода на страницу оплаты',
				'shortname' => 'URL перехода', column => 5,
				'rem' => 'техническое поле, заполняется программистом' },
#		{ 'attr' => 'handler',		'type' => 'string',		'rusname' => 'Обработчик', rem => 'техническое поле, заполняется программистом' },
	)
}


sub class_name
{
	return 'Способ оплаты';
}

sub class_description
{
	return 'Способ оплаты';
}

sub contenido_status_style
{
        my $self = shift;
        if ( $self->status == 2 ) {
                return 'color:green;';
        }
}

sub pre_store
{
	my $self = shift;

	my $default_section = $project->s_alias->{payment}	if ref $project->s_alias eq 'HASH' && exists $project->s_alias->{payment};
	my $sections = $self->{sections};
	if ( $default_section ) {
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($sections, $default_section);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}

1;
