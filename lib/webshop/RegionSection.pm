package webshop::RegionSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
#		{ 'attr' => 'default_document_class',		'default' => 'webshop::Delivery' },
#		{ 'attr' => '_sorted',				'default' => 1 },
		{ 'attr' => 'filters',				'hidden' => 1 },
	)
}

sub class_name
{
	return 'География доставки';
}

sub class_description
{
	return 'География доставки';
}

1;
