package webshop::CouponItemLink;

use base 'Contenido::Link';
use Contenido::Globals;

sub class_name
{
	return 'Купон к товару';
}

sub class_description
{
	return 'Купон к товару';
}

sub extra_properties
{
	return ();
}

sub class_table
{
	return 'webshop::SQL::CouponLinkTable';
}


sub available_sources
{
	return [ qw(webshop::Coupon) ];
}

sub available_destinations
{
	my $self = shift;
	return $state->{webshop}->{item_document_class};
}

1;
