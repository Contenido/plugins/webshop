package webshop::DiscountItemLink;

use base 'Contenido::Link';
use Contenido::Globals;

sub class_name
{
	return 'Скидка на товар';
}

sub class_description
{
	return 'Скидка на товар';
}

sub extra_properties
{
	return ();
}

sub class_table
{
	return 'webshop::SQL::CouponLinkTable';
}


sub available_sources
{
	return [ qw(webshop::Discount) ];
}

sub available_destinations
{
	my $self = shift;
	return $state->{webshop}->{item_document_class};
}

1;
