package webshop::Area;

use Contenido::Globals;
use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус',
			'cases' => [
					[0, 'Запись не активна'],
					[1, 'Запись активна'],
				],
		},
		{ 'attr' => 'pid',	'type' => 'lookup',	'rusname' => 'Страна',
				lookup_opts => { class => 'webshop::Country', order_by => 'name' },
				allow_null => 1,
		},
	)
}

sub class_name
{
	return 'Webshop: регион';
}

sub class_description
{
	return 'Webshop: region';
}

sub class_table
{
	return 'webshop::SQL::RegionsTable';
}

sub table_links
{
	return [
		{ name => 'Города', class => 'webshop::Town', filter => 'pid', field => 'pid' },
	];
}

sub pre_store
{
	my $self = shift;

	unless ( $self->pid ) {
		my $count = $self->keeper->get_documents (
					class	=> 'webshop::Country',
					count	=> 1,
				);
		if ( $count == 1 ) {
			my ($country) = $self->keeper->get_documents (
					class	=> 'webshop::Country',
				);
			$self->pid( $country->id );
		}
	}

	my $default_section = $project->s_alias->{webshop_area}     if ref $project->s_alias eq 'HASH';
	my $sections = $self->{sections};
	if ( $default_section ) {
		if ( ref $sections eq 'ARRAY' && scalar @$sections ) {
			my @new_sects = grep { $_ != $default_section } @$sections;
			push @new_sects, $default_section;
			$self->sections(@new_sects);
		} elsif ( $sections && !ref $sections && $sections != $default_section ) {
			my @new_sects = ($default_section, $sections);
			$self->sections(@new_sects);
		} else {
			$self->sections($default_section);
		}
	}

	return 1;
}

1;
