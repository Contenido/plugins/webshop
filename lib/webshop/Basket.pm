package webshop::Basket;

use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус заказа',
			'cases' => [
					[0, 'Элемент wish-листа'],
					[1, 'Элемент корзины'],
				],
		},
		{ 'attr' => 'articul',		'type' => 'string',	'rusname' => 'Артикул' },
		{ 'attr' => 'colour',		'type' => 'string',	'rusname' => 'Цвет' },
		{ 'attr' => 'size',		'type' => 'string',	'rusname' => 'Размер' },
		{ 'attr' => 'discount_flag',	'type' => 'string',	'rusname' => 'Наличие скидки' },
		{ 'attr' => 'discount',		'type' => 'text',       'rusname' => 'Формула скидки', rows => 15 },
	)
}

sub price_formatted {
	my $self = shift;

	my $price = $self->price;
	$price = reverse $price;
	$price =~ s/(\d{3})/$1\ /g;
	$price = reverse $price;

	return $price;
}

sub total_formatted {
	my $self = shift;

	my $price = $self->price * ($self->number || 0);
	$price = reverse $price;
	$price =~ s/(\d{3})/$1\ /g;
	$price = reverse $price;

	return $price;
}

sub class_name
{
	return 'Позиция в корзине';
}

sub class_description
{
	return 'Позиция в корзине';
}

sub class_table
{
	return 'webshop::SQL::Basket';
}

1;
