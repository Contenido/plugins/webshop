package webshop::PaymentSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
		{ 'attr' => 'default_document_class',		'default' => 'webshop::Payment' },
		{ 'attr' => '_sorted',				'default' => 1 },
		{ 'attr' => 'filters',				'hidden' => 1 },
		{ 'attr' => 'order_by',				'hidden' => 1 },
	)
}

sub class_name
{
	return 'Cпособы оплаты';
}

sub class_description
{
	return 'Cпособы оплаты';
}

1;
