CREATE TABLE orders (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    class text DEFAULT 'webshop::Order'::text NOT NULL,
    status smallint default 0 NOT NULL,
    payment smallint default 0,
    syncro smallint default 0,
    synctime timestamp without time zone,
    sections integer[],
    uid integer,
    company_id integer,
    manager_id integer,
    vault_id integer,
    postman_id integer,
    session text,
    name text,
    data text
);

CREATE INDEX orders_sections ON orders USING gist (sections);
CREATE INDEX orders_session ON orders USING btree (session) where session is not null;
CREATE INDEX orders_uid ON orders USING btree (uid) where uid is not null;
