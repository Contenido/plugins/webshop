CREATE TABLE webshop_order_coupons (
	id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
	ctime timestamp without time zone DEFAULT now() NOT NULL,
	mtime timestamp without time zone DEFAULT now() NOT NULL,
	class text DEFAULT 'webshop::OrderCouponLink'::text NOT NULL,
	status smallint default 0 NOT NULL,
	uid integer default 0,
	session text,
	source_id integer,
	source_class text default 'webshop::Order'::text,
	dest_id integer,
	dest_class text default 'webshop::Coupon'::text,
	data text
);

CREATE INDEX webshop_order_coupons_user ON webshop_order_coupons USING btree (uid, session);
CREATE UNIQUE INDEX webshop_order_coupons_in_order ON webshop_order_coupons USING btree (source_id, dest_id) where source_id > 0;
create index webshop_order_coupons_source on webshop_order_coupons (source_id);
create index webshop_order_coupons_dest on webshop_order_coupons (dest_id);
