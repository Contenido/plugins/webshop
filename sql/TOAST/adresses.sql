CREATE TABLE adresses (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    class text DEFAULT 'webshop::Address'::text NOT NULL,
    status smallint default 0 NOT NULL,
    sections integer,
    uid integer not null,
    delivery_id integer references webshop_delivery(id) ON DELETE RESTRICT,
    name text,
    data text
);

CREATE INDEX adresses_sections ON adresses USING btree (sections);
CREATE INDEX adresses_uid ON adresses USING btree (uid);
