CREATE TABLE webshop_regions (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    class text NOT NULL,
    status smallint default 0 NOT NULL,
    sections integer[],
    pid integer default 0,
    name text,
    alias text,
    price text,
    data text
);

CREATE INDEX webshop_regions_sections ON webshop_regions USING btree (sections);
CREATE INDEX webshop_regions_pid ON webshop_regions USING btree (pid);
