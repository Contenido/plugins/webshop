CREATE TABLE basket (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    class text DEFAULT 'webshop::Basket'::text NOT NULL,
    status smallint default 0 NOT NULL,
    sections integer[],
    uid integer,
    item_id integer not null,
    item_class text,
    item_table text,
    order_id integer,
    color_id integer,
    size_id integer,
    session text,
    number integer default 0,
    price float default 0,
    special_price integer default 0,
    name text,
    data text
);

CREATE INDEX basket_order_id ON basket USING btree (order_id);
CREATE INDEX basket_sections ON basket USING gist (sections);
CREATE INDEX basket_session ON basket USING btree (session) where session is not null;
CREATE INDEX basket_uid ON basket USING btree (uid);
