CREATE TABLE webshop_delivery (
    id integer NOT NULL primary key DEFAULT nextval(('public.documents_id_seq'::text)::regclass),
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    class text DEFAULT 'webshop::Delivery'::text NOT NULL,
    status smallint default 0 NOT NULL,
    sections integer[],
    name text,
    alias text,
    data text
);

create index webshop_delivery_sections on webshop_delivery using gist ( "sections" "gist__int_ops" );
