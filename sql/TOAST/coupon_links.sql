create table webshop_coupon_links
(
	id integer not null primary key default nextval('public.documents_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	status smallint not null default 1,
	source_id integer not null,
	source_class text not null default 'webshop::Coupon',
	dest_id integer not null,
	dest_class text not null,
	data text
);
create index webshop_coupon_links_source on webshop_coupon_links (source_id);
create index webshop_coupon_links_dest on webshop_coupon_links (dest_id);
