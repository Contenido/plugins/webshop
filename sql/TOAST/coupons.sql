CREATE TABLE webshop_coupons (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    etime timestamp without time zone DEFAULT now() NOT NULL,
    class text DEFAULT 'webshop::Coupon'::text NOT NULL,
    status smallint default 0 NOT NULL,
    sections integer,
    pid integer default 0,
    uid integer default 0,
    groups integer[],
    name text,
    code text not null,
    data text
);

CREATE INDEX webshop_coupons_sections ON webshop_coupons USING btree (sections);
CREATE INDEX webshop_coupons_uid ON webshop_coupons USING btree (uid);
CREATE INDEX webshop_coupons_code ON webshop_coupons USING btree (code);
